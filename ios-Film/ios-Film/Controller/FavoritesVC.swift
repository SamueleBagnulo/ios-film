//
//  FavoritesVC.swift
//  ios-Film
//
//  Created by SERIOP on 10/09/21.
//

import UIKit

class FavoritesVC: UIViewController {

    @IBOutlet weak var favTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension FavoritesVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AppData.shared.favFilm.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = favTableView.dequeueReusableCell(withIdentifier: "favCell", for: indexPath)
        
        cell.textLabel?.text = AppData.shared.favFilm[indexPath.row].title
        return cell
    }
    
    
}
