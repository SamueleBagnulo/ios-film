//
//  PanelVC.swift
//  ios-Film
//
//  Created by BAGNULOS on 07/09/21.
//

import UIKit

enum PanelState {
    case expanded, compact, dismissed
}

class PanelVC: UIViewController {
    
    @IBOutlet weak var favButton: UIButton!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var imageWidthConstraint: NSLayoutConstraint!
    @IBOutlet private weak var filmImageView: UIImageView!
    @IBOutlet private weak var regiaLabel: UILabel!
    @IBOutlet private weak var actorsLabel: UILabel!
    @IBOutlet private weak var ratingLabel: UILabel!
    @IBOutlet private weak var plotTextView: UITextView!
    @IBOutlet private weak var expandedTitleLabel: UILabel!
    @IBOutlet private weak var infoStackView: UIStackView!
    @IBOutlet private weak var yearLabel: UILabel!
    @IBOutlet private weak var genreLabel: UILabel!
    
    var filmIndex: Int?
    var panelState = PanelState.dismissed {
        didSet {
            guard let parent = parent else {return}
            refreshUI(parent)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let swipeGRUp = UISwipeGestureRecognizer(target: self, action: #selector(didSwipe(_:)))
        let swipeGRDown = UISwipeGestureRecognizer(target: self, action: #selector(didSwipe(_:)))
        
        swipeGRUp.direction = .up
        swipeGRDown.direction = .down
        view.addGestureRecognizer(swipeGRUp)
        view.addGestureRecognizer(swipeGRDown)
        
        titleLabel.adjustsFontSizeToFitWidth = true
        expandedTitleLabel.adjustsFontSizeToFitWidth = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(didReceiveDetails), name: .didDownloadMoreInfo, object: nil)
    }
    
    @IBAction func favButtonPressed(_ sender: UIButton) {
        if let safeIndex = filmIndex {
            AppData.shared.film[safeIndex].isFavourite.toggle()
            refreshFav()
            if AppData.shared.film[safeIndex].isFavourite &&  AppData.shared.favFilm.filter({$0.imdbID == AppData.shared.film[safeIndex].imdbID}).isEmpty {
                    AppData.shared.favFilm.append(AppData.shared.film[safeIndex])
                    AppData.shared.reloadData()
                print("Add: \(AppData.shared.favFilm.map{$0.imdbID})")
            } else if !AppData.shared.film[safeIndex].isFavourite {
                let film = AppData.shared.film[safeIndex]
                let i = AppData.shared.favFilm.firstIndex(where: {$0.imdbID == film.imdbID})
                AppData.shared.favFilm.remove(at: i!)
                AppData.shared.reloadData()
                print("Remove: \(AppData.shared.favFilm.map{$0.imdbID})")
            }
        }
    }
    
    private func refreshFav() {
        if let safeIndex = filmIndex {
            favButton.setImage(AppData.shared.film[safeIndex].isFavourite ? UIImage(systemName: "heart.fill") : UIImage(systemName: "heart") , for: .normal)
        }
    }
    
    @objc private func didSwipe(_ sender: UISwipeGestureRecognizer) {
        if sender.direction == .down {
            if panelState == .compact {
                panelState = .dismissed
            } else {
                panelState = .compact
            }
        } else if sender.direction == .up {
            if panelState == .compact {
                panelState = .expanded
            }
        }
    }
    
    func configure(with index: Int) {
        filmIndex = index
        let film = AppData.shared.film[index]
        titleLabel.text = film.title
        if let safeFilm = film.imageData {
            filmImageView.image = UIImage(data: safeFilm)
        }
        yearLabel.text = film.year
        
        // check se è salvato in memoria, allora il cuore è riempito
        if !AppData.shared.favFilm.filter({$0.imdbID == film.imdbID}).isEmpty {
            AppData.shared.film[index].isFavourite = true
            self.favButton.setImage(UIImage(systemName: "heart.fill"), for: .normal)
            print(film.isFavourite)
            print(AppData.shared.film[index].isFavourite)
        } else {
            AppData.shared.film[index].isFavourite = false
            self.favButton.setImage(UIImage(systemName: "heart"), for: .normal)
        }
    }
    
    @objc func didReceiveDetails() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self, let i = self.filmIndex else { return }
            let film = AppData.shared.film[i]
            self.genreLabel.text = film.genre
            self.plotTextView.text = film.plot
            self.actorsLabel.text = film.actors
            self.ratingLabel.text = film.rating
            self.regiaLabel.text = film.director
        }
    }
    
    private func refreshUI(_ parent: UIViewController) {
        imageWidthConstraint.constant = panelState == .expanded ? 200 : 100
        UIView.animate(withDuration: 0.3) { [self] in
            switch panelState {
            case .expanded:
                view.frame.size.height = (parent.view.frame.height/3)*2.7
                infoStackView.distribution = .fillEqually
                expandedTitleLabel.text = titleLabel.text
                titleLabel.isHidden = true
                regiaLabel.isHidden = false
                actorsLabel.isHidden = false
                ratingLabel.isHidden = false
            case .compact:
                view.frame.size.height = parent.view.frame.height/3
                infoStackView.distribution = .fillProportionally
                titleLabel.isHidden = false
                expandedTitleLabel.text = ""
                regiaLabel.isHidden = true
                actorsLabel.isHidden = true
                ratingLabel.isHidden = true
            case .dismissed:
                view.frame.size.height = 0
                expandedTitleLabel.text = ""
            }
            
            view.layoutIfNeeded()
            view.frame.origin.y = parent.view.frame.height - view.frame.height
        }
    }
}
