//
//  ViewController.swift
//  ios-Film
//
//  Created by BAGNULOS on 03/09/21.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet private weak var searchButton: UIButton!
    @IBOutlet private weak var favButton: UIButton!
    @IBOutlet private weak var tableView: UITableView!
    
    var panelVC: PanelVC!
    
    lazy private var apiManager: ApiManager = {
        let apiManager = ApiManager()
        apiManager.delegate = self
        return apiManager
    }()
    
    private var genres = [
        "Movies",
        "Series"
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        title = "Lista film"
        
        setShadowButton(for: searchButton, favButton)
        
        let nib = UINib(nibName: "SectionCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "sectionCell")
        
        apiManager.fetchDataBySearch(searchString: "harry")
        
        let sb = UIStoryboard(name: "Main", bundle: nil)
        panelVC = sb.instantiateViewController(identifier: "panelVC")
        
        panelVC.view.layer.masksToBounds = true
        panelVC.view.layer.cornerRadius = 10
        panelVC.view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        addChild(panelVC)
        view.addSubview(panelVC.view)
        panelVC.view.frame = CGRect(x: 0, y: view.frame.height,
                                    width: view.frame.width, height: 0)
        let tapGesture =
            UITapGestureRecognizer(target: self, action: #selector(didTapInView(_:)))
        tapGesture.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc private func didTapInView(_ gesture: UITapGestureRecognizer) {
        let location = gesture.location(in: view)
        if !panelVC.view.frame.contains(location) {
            if panelVC.panelState == .compact {
                panelVC.panelState = .dismissed
            }
        }
    }
    
    private func setShadowButton(for buttons: UIButton...) {
        buttons.forEach {
            $0.layer.shadowColor = UIColor.black.cgColor
            $0.layer.shadowOffset = CGSize(width: 0, height: 6)
            $0.layer.shadowRadius = 5
            $0.layer.shadowOpacity = 0.5
        }
    }
    
    
    @IBAction func favDidPress(_ sender: UIButton) {
        
        
    }
    
    
    @IBAction func searchDidPress(_ sender: UIButton) {
        tableView.reloadData()
    }
}

//MARK: - UITableView

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "sectionCell", for: indexPath) as! SectionCell
        
        cell.delegate = self
        cell.configure(with: genres[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        200
    }
}

//MARK: - ApiManagerDelegate

extension ViewController: ApiManagerDelegate {
    func didFailWithError(error: Error) {
        print(error.localizedDescription)
    }
    
    func didSuccess(listFilm: [Search]) {
        AppData.shared.film = listFilm.map { Film(search: $0) }
        NotificationCenter.default.post(name: .didReceiveFilms, object: self)
    }
}

//MARK: - SelectCellDelegate

extension ViewController: SelectCellDelegate {
    func didSelectFilm(at index: Int) {
        panelVC.configure(with: index)
        panelVC.panelState = .compact
    }
}
