//
//  Extensions.swift
//  ios-Film
//
//  Created by BAGNULOS on 03/09/21.
//

import UIKit

extension UIColor {
    static var random: UIColor {
        return UIColor(
            red: .random(in: 0...1),
            green: .random(in: 0...1),
            blue: .random(in: 0...1),
            alpha: 1.0
        )
    }
}

extension NSNotification.Name {
    static let didReceiveFilms = NSNotification.Name(rawValue: "didReceiveFilms")
    static let didDownloadMoreInfo = NSNotification.Name("didDownloadMoreInfo")
}
