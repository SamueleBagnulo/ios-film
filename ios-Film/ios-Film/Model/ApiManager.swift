//
//  ApiManager.swift
//  ios-Film
//
//  Created by PALMIOTTAA on 06/09/21.
//

import Foundation

class ApiManager {
    
    private let baseURL = "https://www.omdbapi.com/"
    private let apiKey = "cb8c8cbe"
    
    weak var delegate: ApiManagerDelegate?
    
    func fetchDataBySearch(searchString: String) {
        let url = "\(baseURL)?apikey=\(apiKey)&s=\(searchString)"
        performRequest(with: url)
    }
    
    func performRequest(with urlString: String) {
        if let safeURL = URL(string: urlString) {
            let session = URLSession(configuration: .default)
            let task = session.dataTask(with: safeURL, completionHandler: {
                (data, response, error) in
                if let error = error {
                    // didFailMethodDelegate
                    self.delegate?.didFailWithError(error: error)
                    return
                }
                if let safeData = data {
                    // parseJson method
                    if urlString.contains("&s=") {
                        if let safeFilmBySearchModel = self.parseDataBySearch(data: safeData) {
                            DispatchQueue.main.async {
                                self.delegate?.didSuccess(listFilm: safeFilmBySearchModel.listFilm)
                            }
                        }
                    } else if urlString.contains("&i=") {
                        if let filmDetailsModel = self.parseDataByID(data: safeData) {
                            AppData.shared.updateFilm(with: filmDetailsModel)
                        }
                    }
                }
            })
            task.resume()
        }
    }
    
    func downloadMoreInfo(id: String) {
        let url = "\(baseURL)?apikey=\(apiKey)&i=\(id)"
        performRequest(with: url)
    }
    
    private func parseDataBySearch(data: Data) -> FilmBySearchModel? {
        let decoder = JSONDecoder()
        
        do {
            let decodedData = try decoder.decode(SearchFilmDTO.self, from: data)
            let filmBySearch = FilmBySearchModel(listFilm: decodedData.Search)
            return filmBySearch
        } catch {
            delegate?.didFailWithError(error: error)
            return nil
        }
    }
    
    private func parseDataByID(data: Data) -> FilmDetailsModel? {
        let decoder = JSONDecoder()
        
        do {
            let decodedData = try decoder.decode(FilmDetailsDTO.self, from: data)
            let filmBySearch = FilmDetailsModel(filmDTO: decodedData)
            return filmBySearch
        } catch {
            delegate?.didFailWithError(error: error)
            return nil
        }
    }
    
}

protocol ApiManagerDelegate: AnyObject {
    func didFailWithError(error: Error)
    func didSuccess(listFilm: [Search])
}
