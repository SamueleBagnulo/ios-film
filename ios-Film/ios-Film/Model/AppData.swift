//
//  AppData.swift
//  ios-Film
//
//  Created by PALMIOTTAA on 06/09/21.
//

import Foundation

extension UserDefaults {
    func decode<T: Decodable>(_ type: T.Type, forKey defaultName: String) throws -> T {
        try JSONDecoder().decode(T.self, from: data(forKey: defaultName) ?? .init())
    }
    
    func encode<T: Encodable>(_ value: T, forKey defaultName: String) throws {
        try set(JSONEncoder().encode(value), forKey: defaultName)
    }
}

final class AppData {
    private init() {}
    
    static let shared = AppData()
    var film = [Film]()
    var favFilm: [Film] {
        get {
            let decodedFavFilms = try? UserDefaults.standard.decode([Film].self, forKey: "savedFavFilms")
            return decodedFavFilms ?? []
        }
        
        set {
            try? UserDefaults.standard.encode(newValue, forKey: "savedFavFilms")
        }
    }
    
    func reloadData() {
        try? UserDefaults.standard.encode(favFilm, forKey: "savedFavFilms")
    }
    
    func updateFilm(with model: FilmDetailsModel) {
        if let index = film.firstIndex(where: { $0.imdbID == model.imdbID }) {
            film[index].getInfo(from: model)
        }
    }
}

