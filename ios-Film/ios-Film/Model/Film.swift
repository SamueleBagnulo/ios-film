//
//  Film.swift
//  ios-Film
//
//  Created by BAGNULOS on 03/09/21.
//

import Foundation

struct Film: Codable {
    let title: String
    let year: String
    let imdbID: String
    let type: String
    // let poster: String
    var plot: String?
    var genre: String?
    var actors: String?
    var director: String?
    var rating: String?
    var imageData: Data?
    var isFavourite: Bool
    
    init(search: Search) {
        title = search.Title
        year = search.Year
        imdbID = search.imdbID
        type = search.Type
        isFavourite = false
        if let url = URL(string: search.Poster) {
            imageData = try? Data(contentsOf: url)
        }
    }
    
    func downloadMoreInfo() {
        ApiManager().downloadMoreInfo(id: imdbID)
    }
    
    mutating func getInfo(from model: FilmDetailsModel) {
        plot = model.plot
        genre = model.genre
        actors = model.actors
        director = model.director
        rating = model.imdbRating
        
        NotificationCenter.default.post(name: .didDownloadMoreInfo, object: self)
    }
}
