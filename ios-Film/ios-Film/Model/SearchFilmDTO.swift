//
//  SearchFilmDTO.swift
//  ios-Film
//
//  Created by PALMIOTTAA on 06/09/21.
//

import Foundation

struct SearchFilmDTO: Codable {
    let Search: [Search]
}

struct Search: Codable {
    let Title: String
    let Year: String
    let imdbID: String
    let `Type`: String
    let Poster: String
}

struct FilmDetailsDTO: Codable {
    let imdbID: String
    let imdbRating: String
    let Genre: String
    let Director: String
    let Actors: String
    let Plot: String
}

struct FilmBySearchModel {
    let listFilm: [Search]
}

struct FilmDetailsModel {
    let imdbID: String
    let imdbRating: String
    let genre: String
    let director: String
    let actors: String
    let plot: String
    
    init(filmDTO: FilmDetailsDTO) {
        imdbRating = filmDTO.imdbRating
        genre = filmDTO.Genre
        director = filmDTO.Director
        actors = filmDTO.Actors
        plot = filmDTO.Plot
        imdbID = filmDTO.imdbID
    }
}
