//
//  filmCell.swift
//  ios-Film
//
//  Created by BAGNULOS on 03/09/21.
//

import UIKit

class FilmCell: UICollectionViewCell {

    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var filmImage: UIImageView!
    @IBOutlet private weak var bookmarkImageView: UIImageView!
    
    func configure(with film: Film) {
        titleLabel.text = film.title
        if let safeData = film.imageData {
            filmImage.image = UIImage(data: safeData)
        }
    }
}

