//
//  SectionCell.swift
//  ios-Film
//
//  Created by BAGNULOS on 03/09/21.
//

import UIKit

protocol SelectCellDelegate: AnyObject {
    func didSelectFilm(at index: Int)
}

class SectionCell: UITableViewCell {
    
    @IBOutlet weak var sectionCollectionView: UICollectionView!
    @IBOutlet weak var sectionTitleLabel: UILabel!
        
    var delegate: SelectCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        sectionCollectionView.delegate = self
        sectionCollectionView.dataSource = self
        
        // Initialization code
        let nib = UINib(nibName: "FilmCell", bundle: nil)
        sectionCollectionView.register(nib, forCellWithReuseIdentifier: "filmCell")
        
        NotificationCenter.default.addObserver(self, selector: #selector(shouldReload), name: .didReceiveFilms, object: nil)
    }
    
    @objc private func shouldReload() {
        sectionCollectionView.reloadData()
    }
    
    func configure(with text: String) {
        sectionTitleLabel.text = text
    }
}

extension SectionCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        AppData.shared.film.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "filmCell", for: indexPath) as! FilmCell
        cell.configure(with: AppData.shared.film[indexPath.row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let film = AppData.shared.film[indexPath.row]
        if film.plot == nil {
            film.downloadMoreInfo()
        }
        delegate?.didSelectFilm(at: indexPath.row)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 85, height: 150)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }
    
}
